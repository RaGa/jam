# Jam

Jam is an Open Source alternative to Clubhouse, Twitter Spaces and similar audio spaces.

With Jam you can create `Jams` which are audio rooms that can be used for panel discussions, jam sessions, free flowing conversations, debates, theatre plays, musicals and more. The only limit is your imagination.

# Features

🙈 no camera video

📬 no direct messages

🪟 no screen sharing

# Develop

In in the `ui` directory use `yarn` to install dependencies and `yarn start` to start the local development server.

Directory overview:

`deployment`/ docker compose file for deploying and hosting of Jam

`pantry`/ a lightweight server for handling authentication and coordination of Jam

`signalhub`/ a simple server for managing WebRTC connections for Jam

`ui`/ web based user interface based on the React framework